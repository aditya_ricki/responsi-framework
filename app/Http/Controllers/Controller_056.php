<?php

namespace App\Http\Controllers;

use App\Models\Model_056;
use Illuminate\Http\Request;
use App\Http\Requests\RequestTb;

class Controller_056 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Model_056::paginate(6);

        return view('056.056_display', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('056.056_form_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestTb $request)
    {
        Model_056::create($request->all());

        return redirect()->back()->with('success', 'Data created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Model_056  $model_056
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Model_056::find($id);

        return view('056.056_form_edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Model_056  $model_056
     * @return \Illuminate\Http\Response
     */
    public function update(RequestTb $request, $id)
    {
        Model_056::find($id)->update($request->all());

        return redirect()->back()->with('success', 'Data updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Model_056  $model_056
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Model_056::find($id)->delete();

        return redirect()->back()->with('success', 'Data deleted!');
    }
}
