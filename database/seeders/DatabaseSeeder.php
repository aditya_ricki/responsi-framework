<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Model_056;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Model_056::factory(300)->create();
    }
}
