<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Model_056 extends Model
{
    use HasFactory;

	protected $table    = '056_tb';

	protected $fillable = [
		'name_056',
		'address_056',
	];
}
