<?php

namespace Database\Factories;

use App\Models\Model_056;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class Model_056Factory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model_056::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name_056'    => $this->faker->name,
            'address_056' => $this->faker->address,
        ];
    }
}
