@extends('056.056_main')

@section('title', 'Display data')

@section('content')
	<div class="card-header">
		<nav class="navbar navbar-light bg-light">
			<div class="container-fluid">
				<a class="navbar-brand" href="056">
				<img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="30" height="24" class="d-inline-block align-top">
					Framework Praktik
				</a>
				<a href="{{ route('056_form_create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add</a>
			</div>
		</nav>
	</div>
	<div class="card-body">
		@if(session('success'))
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<strong>{{ session('success') }}</strong>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
        @endif
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th scope="col">NO</th>
						<th scope="col">Name</th>
						<th scope="col">Address</th>
						<th scope="col">Create</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $dt)
					<tr>
						<th scope="row">{{ $loop->iteration }}</th>
						<td>{{ $dt->name_056 }}</td>
						<td>{{ $dt->address_056 }}</td>
						<td>{{ date('D, d M Y', strtotime($dt['created_at'])) }}</td>
						<td>
							<form action="{{ route('056_destroy', $dt->id) }}" method="post">
								<div class="btn-group" role="group" aria-label="Basic example">
									<a href="{{ route('056_form_edit', $dt->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i> Edit</a>
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i> Delete</button>
								</div>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<div class="card-footer">
		<div class="d-flex">
			{!! $data->links() !!}
		</div>
	</div>
@stop