@extends('056.056_main')

@section('title', 'Create data')

@section('content')
	<div class="card-header">
		<nav class="navbar navbar-light bg-light">
			<div class="container-fluid">
				<a class="navbar-brand" href="056">
				<img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo.svg" alt="" width="30" height="24" class="d-inline-block align-top">
					Framework Praktik
				</a>
				<a href="{{ route('056_display') }}" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Back</a>
			</div>
		</nav>
	</div>
	<form action="{{ route('056_insert') }}" method="post">
		@csrf
		@method('post')
		<div class="card-body">
			@if(session('success'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<strong>{{ session('success') }}</strong>
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				</div>
            @endif
			<div class="mb-3">
				<label for="name_056" class="form-label">Name</label>
				<input type="text" class="form-control{{ $errors->has('name_056') ? ' is-invalid' : '' }}" name="name_056" id="name_056" placeholder="Name" value="{{ old('name_056') }}" aria-describedby="validationName">
				@error('name_056')
					<div id="validationName" class="invalid-feedback">
						{{ $message }}
					</div>
				@enderror
			</div>
			<div class="mb-3">
				<label for="address_056" class="form-label">Address</label>
				<textarea class="form-control{{ $errors->has('address_056') ? ' is-invalid' : '' }}" name="address_056" id="address_056" rows="3" placeholder="Address">{{ old('address_056') }}</textarea aria-describedby="validationAddress">
				@error('address_056')
					<div id="validationAddress" class="invalid-feedback">
						{{ $message }}
					</div>
				@enderror
			</div>
		</div>
		<div class="card-footer">
			<button type="submit" class="btn btn-success"><i class="fas fa-save"></i> Save</button>
		</div>
	</form>
@stop