<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('responsi', App\Http\Controllers\Controller_056::class, [
	'except' => [
		'show',
	],
	'names'  => [
		'index'   => '056_display',
		'create'  => '056_form_create',
		'store'   => '056_insert',
		'edit'    => '056_form_edit',
		'update'  => '056_update',
		'destroy' => '056_destroy',
	]
]);
